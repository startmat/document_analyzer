from __future__ import unicode_literals

DO_ANALYZER_RETRY_DELAY = 10
LOCK_EXPIRE = 60 * 10  # Adjust to worst case scenario
